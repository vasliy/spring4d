{-----------------------------------------------------------------------------}

unit EhLibObjectDataSet;

{$I EhLib.Inc}

interface

uses
  Classes, Db, SysUtils,
  DbUtilsEh, DBGridEh, ToolCtrlsEh,
  Spring.Data.ObjectDataSet;

type
  TCrackObjectDataSet = class(TObjectDataSet);

  TFDDatasetFeaturesEh = class(TSQLDatasetFeaturesEh)
  private
    procedure ODataSetLocalSort(AGrid: TCustomDBGridEh; ADataSet: TObjectDataSet);
  public
    constructor Create; override;
    procedure ApplyFilter(ASender: TObject; ADataSet: TDataSet; AIsReopen: Boolean); override;
    procedure ApplySorting(ASender: TObject; ADataSet: TDataSet; AIsReopen: Boolean); override;
    procedure FillSTFilterListDataValues(AGrid: TCustomDBGridEh; AColumn: TColumnEh; AItems: TStrings); override;
  end;

//var
//  bFilterWithValues: Boolean;

implementation

uses
  Variants;

function DateValueToFDSQLStringProc(ADataSet: TDataSet; AValue: Variant): String;
begin
  Result := DateValueToDataBaseSQLString('STANDARD', AValue);
end;

constructor TFDDatasetFeaturesEh.Create;
begin
  inherited Create;
  DateValueToSQLString := DateValueToFDSQLStringProc;
end;

procedure TFDDatasetFeaturesEh.ApplyFilter(ASender: TObject; ADataSet: TDataSet;
  AIsReopen: Boolean);
begin
  if (ASender is TCustomDBGridEh) and ADataSet.Active then
    if TDBGridEh(ASender).STFilter.Local then
    begin
      ADataSet.Filter := Trim(GetExpressionAsFilterString(TCustomDBGridEh(ASender),
        GetOneExpressionAsLocalFilterString, DateValueToFDSQLStringProc, False, True));
      ADataSet.Filtered := ADataSet.Filter <> '';
    end
    else
      ApplyFilterSQLBasedDataSet(TCustomDBGridEh(ASender), ADataSet,
        DateValueToFDSQLStringProc, AIsReopen, 'SQL');
end;

procedure TFDDatasetFeaturesEh.ODataSetLocalSort(AGrid: TCustomDBGridEh;
  ADataSet: TObjectDataSet);
var
  i: Integer;
  sIndex, sFieldName: String;
  oField: TField;
begin
  try
    if AGrid.SortMarkedColumns.Count > 0 then
    begin
      sIndex := '';
      for i := 0 to AGrid.SortMarkedColumns.Count - 1 do
      begin
        sFieldName := AGrid.SortMarkedColumns[i].FieldName;
        oField := AGrid.DataSource.DataSet.FieldByName(sFieldName);
        if (oField.FieldKind = fkLookup) and (Pos(';', oField.KeyFields) = 0) then
          sFieldName := oField.KeyFields;
        if sIndex <> '' then
          sIndex := sIndex + ',';
        sIndex := sIndex + sFieldName;
        if AGrid.SortMarkedColumns[i].Title.SortMarker = smDownEh then
          sIndex := sIndex + ' Desc';
      end;
      ADataSet.Sort := sIndex;
    end
    else
      ADataSet.Sort := '';
  except
  end;
end;

procedure TFDDatasetFeaturesEh.ApplySorting(ASender: TObject; ADataSet: TDataSet;
  AIsReopen: Boolean);
begin
  if (ASender is TCustomDBGridEh) and ADataSet.Active then
    if TCustomDBGridEh(ASender).SortLocal then
      ODataSetLocalSort(TCustomDBGridEh(ASender), TObjectDataSet(ADataSet))
    else
    begin
      //inherited ApplySorting(ASender, ADataSet, AIsReopen);
    end;
end;

procedure TFDDatasetFeaturesEh.FillSTFilterListDataValues(AGrid: TCustomDBGridEh;
  AColumn: TColumnEh; AItems: TStrings);
Var
  oDS: TObjectDataSet;
  oField: TField;
  oData: TStringList;
  obj: TObject;
begin
  oDS := AGrid.DataSource.DataSet as TObjectDataSet;
  if {bFilterWithValues and} (AColumn.STFilter.ListSource = nil) and
    (not oDS.IsEmpty) then
  begin
    oField := oDS.FindField(AColumn.FieldName);
    if oField <> nil then
    begin
        oData := TStringList.Create;
        oData.CaseSensitive := True;
        oData.Sorted := True;
        oData.Duplicates := dupIgnore;
        try
          for obj in oDS.DataList do
            oData.Add(VarToStr(TCrackObjectDataSet(oDS).InternalGetFieldValue(oField,obj)));
          oData.Sort;
          AItems.AddObject('-', PopupListboxItemEhLine);
          AItems.AddStrings(oData);
        finally
          oData.Free;
        end;
    end;
  end else
    inherited FillSTFilterListDataValues(AGrid, AColumn, AItems);
end;

initialization
  //bFilterWithValues := False;
  RegisterDatasetFeaturesEh(TFDDatasetFeaturesEh, TObjectDataSet);
end.
